## 书法练习轨迹ReadMe

#### 一、自序

幼时接触书法，学而未成。 <br/>
二零二零庚子年九月底，有感大丈夫处身立世必有一技之长。 <br/>
或曰**君子慎独**，一人之际，奋发之时。尝学英语，练吹埙，打篮球，志不在此，无有所成。 <br/>
历年从事之业，设计、物流、销售、客服，加之当前程序员工作。或天资所限，或志不在此，难登顶峰。 <br/>
快哉，路漫漫其修远兮，吾将上下求索。水落石出，拨云见日，惟书法是吾追求，趣之所向，志之所在，必持之以恒，有所为。自勉。 <br/>
`--2020-11-11`


此为本人书法练习历程： [书法练习轨迹--明月几时有]( https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html )。记录每周练字情况，拍照留存及一些感想。之所以记录下来，主要是以下几点原因：  <br/>
一、是要把所写的字，由丑到美，由差到好的过程保存下来，方便对照。不想练的时候，觉得没有进步的时候，看看以前的丑字又会给自己无穷的动力：原来自己进步蛮大，天天练习还是有意义的。 <br/>
二、是寻找志同道合者，对书法有兴趣的可以联系本人，相互交流，共同进步，所谓他山之石，可以攻玉。同时也希望书法大师不吝赐教，光临指导。 <br/>
三、炫耀+装逼，引流+宣传。毕竟人生在世，名利二字。当然肯定是一年半载甚至三年五载，小有所成之后才能晒图，发朋友圈，示字与人。 <br/>
四、希望究天人之际，通古今之变，成一家书法之言。目前字还是不够好，需继续练习。**坚持，自勉**。 <br/>
`--2021-01-04`

人情必有所寄，习惯而成自然。练着练着，书法就成了生活的一部分，记录书法练习轨迹更是有趣有意思的点缀。不能说是点缀，于我而言，练字与记录是相辅相成、缺一不可的关系。 <br/>
`--2021-07-03`

---

<div STYLE="page-break-after: always;"></div>

#### 二、书法练习轨迹网址

> 若无意外，一般每周一在gitlab《**书法练习轨迹--明月几时有**》记录上周练习情况。其他渠道，不定时同步。

```
以下网址包含三类文件：《书法练习轨迹--明月几时有》、《书法练习轨迹ReadMe》、书法练习轨迹图片。
《书法练习轨迹--明月几时有》记录本人每周练习书法情况：包含练习图片及一些感悟。
《书法练习轨迹ReadMe》即当前文档，是git平台的README文件：作自我介绍之意。
书法练习轨迹图片是《书法练习轨迹--明月几时有》文档中图片实际保存地，markdown文件中看到的图片是图片链接。
注：书法练习轨迹--明月几时有.md  README.md 都是markdown格式的文件。
```

| 序号 | 仓库                                                			      |  备注             			                 |
| ---  | -------------------------------------------------------------        |  -----------------------------------         |
| 1    | [**gitlab**]( https://gitlab.com/xuyq123/calligraphy ) &ensp; [imgs]( https://gitlab.com/xuyq123/imgs )                 		|  国外网站，网速较慢。                            |
| 2    | [csdn_code]( https://codechina.csdn.net/xu180/calligraphy ) &ensp; [imgs]( https://codechina.csdn.net/xu180/imgs )  			|  备份，国内网站，速度快。用户较少。        	   |
| 3    | [github]( https://github.com/scott180/calligraphy ) &ensp; [imgs]( https://github.com/scott180/imgs ) 			  			    |  备份，最流行git仓库。国外网站，但有时打不开。   |
| 4    | [coding]( https://xyqin.coding.net/public/my/calligraphy/git ) &ensp; [imgs]( https://xyqin.coding.net/public/my/imgs/git )	|  备份，速度快。但仓库markdown文件不渲染。		   |
| 5    | [gitee]( https://gitee.com/xy180/calligraphy ) &ensp; [imgs]( https://gitee.com/xy180/imgs )                        			|  备份，国内网站，速度快。但可能会被[屏蔽]。      |
| 6    | [bitbucket]( https://bitbucket.org/xu12345/calligraphy ) &ensp; [imgs]( https://bitbucket.org/xu12345/imgs )                   |  备份，国外网站。markdown渲染不太完善。          |
| -    | **社区**                        |                                    |
| 7    | [csdn博客]( https://blog.csdn.net/xu180/article/details/113602103 ) &ensp; [ReadMe]( https://blog.csdn.net/xu180/article/details/118492424 )  |  程序员技术交流平台，发布文章，有删减。      |
| 8    | [博客园]( https://www.cnblogs.com/scott123/p/14729493.html ) &ensp; [ReadMe]( https://www.cnblogs.com/scott123/p/14972979.html )              |  开发者知识分享社区。                        |
| 9    | [语雀]( https://www.yuque.com/longguang123/ccgbto/cbq9u0 ) &ensp; [ReadMe]( https://www.yuque.com/longguang123/ccgbto/oby4hq )                |  文档与知识管理工具，无删减。阿里巴巴产品。  |
| 10   | [飞书]( https://nal4j8dwi0.feishu.cn/docs/doccntwAAd1yjADzHGQT0ueBkN7 ) &ensp; [ReadMe]( https://nal4j8dwi0.feishu.cn/docs/doccnpf5pWihfu3aW4psk5vO7ue )           | 办公平台。字节跳动产品。|
| -    | **云盘**                        |                                    |
| 11   | [坚果云]( https://www.jianguoyun.com/p/DTnLeQEQxP-NBhjNrfED ) &ensp; [markdown]( https://www.jianguoyun.com/p/DfYHsfUQxP-NBhjOrfED )          |  文件分享。        |
| 12   | [百度网盘]( https://pan.baidu.com/s/1dOJMgeZAyCYolEflsKIOPQ )        | 提取码: zpxu 。pdf文件分享，需要登录。       |
| 13   | [阿里云盘]( https://www.aliyundrive.com/s/dKE1SMhqdwn )              | pdf文件分享，需要登录。                      |
| 14   | [天翼云]( https://cloud.189.cn/t/RRBbumb2MB7b )                      | pdf文件分享，需要登录。中国电信网盘。        |
| 15   | [和彩云]( https://caiyun.139.com/m/i?125CmrCy7hU1y )                 | 提取码:WAmq 。pdf文件分享，需要登录。中国移动网盘。     |
| 16   | [wps云盘]( https://www.kdocs.cn/l/cpUDGjX6765H )                     | pdf文件分享，需要登录。                      |
| 17   | [微云]( https://share.weiyun.com/JKZ4ANJ5 )  &ensp; [腾讯文档]( https://docs.qq.com/pdf/DVmxKTG5YZHZBUGlx )         | pdf文件分享。                                |
| 18   | [有道云]( http://note.youdao.com/s/V7b1jHjB )                        | 笔记分享，无删减。               	         |
| 19   | [google云盘]( https://drive.google.com/file/d/1Ubx-Rz3Xwhn48PEXMx-BmWrJGyIAzNfn/view?usp=sharing )                  | 文件分享，无删减。                           |
| -    | **网页**                        |                                    |
| 20   | [作业部落]( https://www.zybuluo.com/scott180/note/1793757 ) &ensp; [ReadMe]( https://www.zybuluo.com/scott180/note/892814 )    | markdown编辑器，文件分享。        |
| 21   | [gitee_pages]( http://xy180.gitee.io/plain-mkdocs/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html ) &ensp; [ReadMe]( http://xy180.gitee.io/plain-mkdocs/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9ReadMe.html ) | gitee静态网页，markdown转html。     |
| 22   | [github_pages]( https://scott180.github.io/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89 ) &ensp; [ReadMe]( https://scott180.github.io/calligraphy ) | github静态网页，有时打不开。[主题1]( https://scott180.github.io/calligraphy1/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89 ) &ensp; [主题2]( https://scott180.github.io/calligraphy2/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89 )  |
| 23   | [**gitlab_pages**]( https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html )  &ensp; [ReadMe]( https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9ReadMe.html )     | gitlab静态网页，markdown转html。       |

---

> git平台mkdocs主题仓库

```
在git平台创建mkdocs主题仓库，自动将markdown文件生成静态网页。 
将《书法练习轨迹--明月几时有》、《书法练习轨迹ReadMe》、《笔名汉字频率分析》等文件聚合在一起。 --20210803
```

| 仓库   | 地址                                               			      |  备注             			                 |
| -----  | -------------------------------------------------------------      |  -----------------------------------         |
| gitlab | [plain-mkdocs]( https://gitlab.com/xuyq123/plain-mkdocs ) &ensp; [**网页**]( https://xuyq123.gitlab.io/plain-mkdocs ) <br/>[plain-pip-mkdocs]( https://gitlab.com/xuyq123/plain-pip-mkdocs ) &ensp; [网页]( https://xuyq123.gitlab.io/plain-pip-mkdocs )  <br/>[plain-mkdocs-gitbook]( https://gitlab.com/xuyq123/plain-mkdocs-gitbook ) &ensp; [网页]( https://xuyq123.gitlab.io/plain-mkdocs-gitbook )            |  gitlab布署mkdocs：配置文件.gitlab-ci.yml。    |
| github | [plain-mkdocs]( https://github.com/scott180/plain-mkdocs ) &ensp; [网页](  https://scott180.github.io/plain-mkdocs/ ) <br/>[plain-pip-mkdocs]( https://github.com/scott180/plain-pip-mkdocs ) &ensp; [网页]( https://scott180.github.io/plain-pip-mkdocs )  <br/>[plain-mkdocs-serve]( https://github.com/scott180/plain-mkdocs-serve )  &ensp; [网页]( https://scott180.github.io/plain-mkdocs-serve )              |  github布署mkdocs：命令mkdocs gh-deploy 。           |
| gitee  |  [plain-mkdocs]( https://gitee.com/xy180/plain-mkdocs ) &ensp; [网页1]( http://xy180.gitee.io/plain-mkdocs/1/site/ ) &ensp; [网页2]( http://xy180.gitee.io/plain-mkdocs/2/site/ ) &ensp; [网页3]( http://xy180.gitee.io/plain-mkdocs/3/site/ )                                  |  gitee布署mkdocs：mkdocs build 布署目录。    |
| note-mkdocs   | gitlab [note-mkdocs]( https://gitlab.com/xuyq123/note-mkdocs )&ensp; [**网页**](  https://xuyq123.gitlab.io/note-mkdocs/ ) &ensp; &ensp; [note-pip-mkdocs]( https://gitlab.com/xuyq123/note-pip-mkdocs )&ensp; [网页](  https://xuyq123.gitlab.io/note-pip-mkdocs/ ) <br/>github [note-mkdocs]( https://github.com/scott180/note-mkdocs )&ensp; [网页](  https://scott180.github.io/note-mkdocs/ )<br/>gitee [plain-mkdocs]( https://gitee.com/xy180/plain-mkdocs/tree/master/note )&ensp; [网页](  http://xy180.gitee.io/plain-mkdocs/note/site/ )       |  个人笔记，含书法练习及java笔记。                        |
| myblog-mkdocs   | gitlab [myblog-mkdocs]( https://gitlab.com/xuyq123/myblog-mkdocs )&ensp; [网页](  https://xuyq123.gitlab.io/myblog-mkdocs/ ) <br/>github [myblog-mkdocs]( https://github.com/scott180/myblog-mkdocs )&ensp; [网页](  https://scott180.github.io/myblog-mkdocs/ )<br/>gitee [myblog-mkdocs]( https://gitee.com/xy180/myblog-mkdocs )      |  我的博客                        |


---

> 笔名汉字频率分析 <br/>
> 数据来自《[书法练习轨迹--明月几时有]( https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html )》中关于本人笔名的记录。

| 序号 | 1    | 2    | 3    | 4    | 5    |
| ---- | ---- | ---- | ---- | ---- | ---- |
|  **链接**   | [gitlab_pages]( https://xuyq123.gitlab.io/plain/%E7%AC%94%E5%90%8D%E6%B1%89%E5%AD%97%E9%A2%91%E7%8E%87%E5%88%86%E6%9E%90 )  |  [语雀]( https://www.yuque.com/longguang123/ccgbto/flt0hf )    | [作业部落]( https://www.zybuluo.com/scott180/note/1809817 ) | [csdn]( https://blog.csdn.net/xu180/article/details/118940065 )   | [github_pages]( https://scott180.github.io/calligraphy/%E7%AC%94%E5%90%8D%E6%B1%89%E5%AD%97%E9%A2%91%E7%8E%87%E5%88%86%E6%9E%90 )   |

---

```

自二零二零年九月底练习书法以来，刚开始一直在gitee仓库记录更新每周练习情况，毕竟这是国产网站，还是要支持的。
后来有鉴于某云盘背信弃义停止服务，一个网站保存资料不保险，鸡蛋不能放到一个篮子里。
而且全世界最流行的git平台是github，于是二零二零年十一月将文件备份到此处，gitee为主github为辅。

但是github是境外网站，网速慢，经常打不开。于是又找了个国产git平台-csdn_code，将资料再备份一份到此处。
这个git平台虽然建立时间端，知名度低，用户少，但是网速挺快，限制也少，不怎么屏蔽关键字。

再后来觉得狡兔三窟，互联网这玩意不是特别靠谱，说不定哪天网站会被封或者停止服务，多找些地方备份总是没错的。
于是网上搜索常用git平台仓库，将书法练习资料又备份到gitlab和coding。这五个平台同时保存书法练习相关资料和图片。

然而随着每周更新，书法练习轨迹的内容越来越多，gitee所屏蔽的关键词也越来越多，还不提示哪个被禁了，应该怎么改。
其他四个平台基本不屏蔽，就他一个屏蔽。总是“该文件疑似存在违规内容，无法显示: 。” 。此处不留人，自有留人处。
因此虽然gitee是第一个记录书法练习资料，有很多历史记录，刚开始一直当做官网，有一种初恋情结。但是不给力不好用啊，只能当备份了。

综合比较这几个git仓库，gitee有关键词屏蔽，根本就看不见内容；github外网速度很慢，经常打不开；csdn_code成立时间短用户少不是很放心；
coding是腾讯的有大厂背书，但是成也tx败也tx，有腾讯微云的前车之鉴，不敢信也不好说，关键是markdown文件不渲染；
还是gitlab比较好用，github第一gitlab就是第二，虽然也是外网，但是网速还行，可以接受。
因此决定将gitlab仓库作为书法练习的官网，每周更新资料，其他的作为备份，不定时同步。

各Git平台书法练习仓库创建时间：
gitee--20200930
github--20201112
csdn_code--20210202
gitlab--20210504
coding--20210524

同时使用阿里云盘、坚果云、百度云盘等等保存练习资料及图片。  
不定时将书法练习轨迹更新到csdn博客、博客园、语雀等比较小众的程序员网站。
以后字写好了能见人了再拿出来到头条、豆瓣、知乎等高流量渠道晒图、晒字。
所谓十年磨一剑，砺得梅花香。

如有游客想浏览本人的书法练习轨迹，可点击上面表格首尾行加粗部分gitlab、gitlab_pages，这两处前者内容最全面，后者格式最美观。

以上 --20210527

```

```

这几天github相关网页都打不开。果然，鸡蛋不能放在一个篮子里。
若要查看书法练习轨迹相关文档，可到上述表格第一号“gitlab”项目。目前这个平台还是稳定的，本人最新记录正在此处。
如感觉网速较慢或者页面加载不出来，可到第二号“csdn_code”或第二十号“作业部落”查看文档。此两处为国产软件，平台稳定，网速较快。
--20210721


github网页还是看不了，csdn_code图片突然打不开，再看看还有啥git平台。
发现bitbucket，在此再备份书法练习轨迹文档及图片。
此平台网速还行，可建立私有仓库，但是对于markdown文件兼容的不理想，有些html标签不能识别，网页渲染不美观。
bitbucket--20210726

```

---

---


<div STYLE="page-break-after: always;"></div>

#### 三、附录

##### 1、书法练习轨迹链接

> git平台 官网  项目地址 静态网页 mkdocs网页 HTTPS  SSH  图片地址  

| 序号 | 官网 | 项目地址 | 静态网页 | mkdocs网页 | HTTPS | SSH | 图片地址 |
| ---  | ---- | -------- | -------- | ---------- | ----- | --- | -------- |
| 1    | [gitlab]( https://gitlab.com/explore/projects/trending ) | [calligraphy]( https://gitlab.com/xuyq123/calligraphy ) | [书法练习轨迹]( https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html ) | [plain-pip-mkdocs]( https://xuyq123.gitlab.io/plain-pip-mkdocs ) | `https://gitlab.com/xuyq123/calligraphy.git` | `git@gitlab.com:xuyq123/calligraphy.git` | [2021050202.jpg]( https://gitlab.com/xuyq123/imgs/-/raw/master/mingyue/2021/202105/2021050202.jpg ) |
| 2    | [csdn_code]( https://codechina.csdn.net/xu180 ) | [calligraphy]( https://codechina.csdn.net/xu180/calligraphy ) | - | - | `https://codechina.csdn.net/xu180/calligraphy.git` | `git@codechina.csdn.net:xu180/calligraphy.git` | [2021042504.jpg]( https://codechina.csdn.net/xu180/imgs/-/raw/master/mingyue/2021/202104/2021042504.jpg ) |
| 3    | [coding]( https://e.coding.net/login ) | [calligraphy]( https://xyqin.coding.net/p/my/d/calligraphy/git ) | [public]( https://xyqin.coding.net/public/my/calligraphy/git/files ) | - | `https://e.coding.net/xyqin/my/calligraphy.git` | `git@e.coding.net:xyqin/my/calligraphy.git` | [2021042601.jpg]( https://xyqin.coding.net/p/my/d/imgs/git/raw/master/mingyue/2021/202105/2021042601.jpg ) |
| 4    | [bitbucket]( https://bitbucket.org/product/ ) | [calligraphy]( https://bitbucket.org/xu12345/calligraphy ) | - | - | `https://bitbucket.org/xu12345/calligraphy.git` | `git@bitbucket.org:xu12345/calligraphy.git` | [2021072402.jpg]( https://bitbucket.org/xu12345/imgs/raw/fac05f5cff30d534a7cf92a9b304b4d7f2df083b/mingyue/2021/202107/2021072402.jpg ) |
| 5    | [github]( https://github.com/scott180 ) | [calligraphy]( https://github.com/scott180/calligraphy ) | [书法练习轨迹]( https://scott180.github.io/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89 ) | [plain-mkdocs]( https://scott180.github.io/plain-mkdocs/ ) | `https://github.com/scott180/calligraphy.git` | `git@github.com:scott180/calligraphy.git` | [2021042501.jpg]( https://raw.githubusercontent.com/scott180/imgs/master/mingyue/2021/202104/2021042501.jpg ) |
| 6    | [gitee]( https://gitee.com/xy180 ) | [calligraphy]( https://gitee.com/xy180/calligraphy ) | [书法练习轨迹]( http://xy180.gitee.io/plain-mkdocs/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html ) | [plain-mkdocs]( http://xy180.gitee.io/plain-mkdocs/1/site ) | `https://gitee.com/xy180/calligraphy.git` | `git@gitee.com:xy180/calligraphy.git` | [2021040202.jpg]( https://gitee.com/xy180/imgs/raw/master/mingyue/2021/202104/2021040202.jpg ) |


```
1、书法练习轨迹链接

《书法练习轨迹--明月几时有》

**仓库** 
gitlab        https://gitlab.com/xuyq123/calligraphy
csdn_code     https://codechina.csdn.net/xu180/calligraphy
github        https://github.com/scott180/calligraphy 
coding        https://xyqin.coding.net/public/my/calligraphy/git
gitee         https://gitee.com/xy180/calligraphy 
bitbucket     https://bitbucket.org/xu12345/calligraphy

**社区**                           
csdn博客      https://blog.csdn.net/xu180/article/details/113602103
博客园        https://www.cnblogs.com/scott123/p/14729493.html
语雀          https://www.yuque.com/longguang123/ccgbto/cbq9u0 
飞书          https://nal4j8dwi0.feishu.cn/docs/doccntwAAd1yjADzHGQT0ueBkN7

**云盘**  
坚果云        https://www.jianguoyun.com/p/DTnLeQEQxP-NBhjNrfED
markdown      https://www.jianguoyun.com/p/DfYHsfUQxP-NBhjOrfED                     
百度网盘      https://pan.baidu.com/s/1dOJMgeZAyCYolEflsKIOPQ   提取码: zpxu  
阿里云盘      https://www.aliyundrive.com/s/dKE1SMhqdwn 
天翼云        https://cloud.189.cn/t/RRBbumb2MB7b
和彩云        https://caiyun.139.com/m/i?125CmrCy7hU1y          提取码:WAmq 
wps云盘       https://www.kdocs.cn/l/cpUDGjX6765H 
微云          https://share.weiyun.com/JKZ4ANJ5   
腾讯文档      https://docs.qq.com/pdf/DVmxKTG5YZHZBUGlx 
有道云        http://note.youdao.com/s/V7b1jHjB 
google云盘    https://drive.google.com/file/d/1Ubx-Rz3Xwhn48PEXMx-BmWrJGyIAzNfn/view?usp=sharing
 
**网页**                       
作业部落      https://www.zybuluo.com/scott180/note/1793757
gitee_pages   http://xy180.gitee.io/plain-mkdocs/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html
github_pages  https://scott180.github.io/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89
gitlab_pages  https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html

---------

《书法练习轨迹ReadMe》

csdn博客      https://blog.csdn.net/xu180/article/details/118492424
博客园        https://www.cnblogs.com/scott123/p/14972979.html
语雀          https://www.yuque.com/longguang123/ccgbto/oby4hq
飞书          https://nal4j8dwi0.feishu.cn/docs/doccnpf5pWihfu3aW4psk5vO7ue
作业部落	  https://www.zybuluo.com/scott180/note/892814
gitee_pages   http://xy180.gitee.io/plain-mkdocs/calligraphy/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9ReadMe.html
github_pages  https://scott180.github.io/calligraphy
gitlab_pages  https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9ReadMe.html
 
---------

书法练习轨迹图片

gitlab        https://gitlab.com/xuyq123/imgs
csdn_code     https://codechina.csdn.net/xu180/imgs
github        https://github.com/scott180/imgs
coding        https://xyqin.coding.net/public/my/imgs/git
gitee         https://gitee.com/xy180/imgs
bitbucket     https://bitbucket.org/xu12345/imgs


---------


gitlab mkdocs主题仓库

plain-mkdocs     
	https://gitlab.com/xuyq123/plain-mkdocs   
	https://xuyq123.gitlab.io/plain-mkdocs   
plain-pip-mkdocs      
	https://gitlab.com/xuyq123/plain-pip-mkdocs  
	https://xuyq123.gitlab.io/plain-pip-mkdocs   
plain-mkdocs-gitbook  
	https://gitlab.com/xuyq123/plain-mkdocs-gitbook  
	https://xuyq123.gitlab.io/plain-mkdocs-gitbook   


github mkdocs主题仓库
plain-mkdocs 
	https://github.com/scott180/plain-mkdocs   
	https://scott180.github.io/plain-mkdocs/

	
gitee mkdocs主题仓库	
	https://gitee.com/xy180/plain-mkdocs/ 	
	http://xy180.gitee.io/plain-mkdocs/1/site
	
	
note-mkdocs   个人笔记，含书法练习及java笔记。
	https://gitlab.com/xuyq123/note-mkdocs    https://xuyq123.gitlab.io/note-mkdocs/
	https://github.com/scott180/note-mkdocs   https://scott180.github.io/note-mkdocs/
	https://gitee.com/xy180/plain-mkdocs/tree/master/note   http://xy180.gitee.io/plain-mkdocs/note/site


```

----

##### 2、各git仓库图片链接

```
2、各git仓库图片链接

gitlab  国外网站，加载慢
https://gitlab.com/xuyq123/imgs/-/raw/master/mingyue/2021/202105/2021050202.jpg

coding  加载快，无屏蔽
https://xyqin.coding.net/p/my/d/imgs/git/raw/master/mingyue/2021/202105/2021042601.jpg

csdn_code  加载快，无屏蔽
https://codechina.csdn.net/xu180/imgs/-/raw/master/mingyue/2021/202104/2021042504.jpg

github  国外网站，加载慢
https://raw.githubusercontent.com/scott180/imgs/master/mingyue/2021/202104/2021042501.jpg

gitee   加载快，有屏蔽
https://gitee.com/xy180/imgs/raw/master/mingyue/2021/202104/2021040202.jpg

bitbucket 国外网站
https://bitbucket.org/xu12345/imgs/raw/fac05f5cff30d534a7cf92a9b304b4d7f2df083b/mingyue/2021/202107/2021072402.jpg

```

---

##### 3、如何将markdown文件导出为带图片的PDF 

```
3、如何将markdown文件导出为带图片的PDF （《书法练习轨迹--明月几时有》是markdown文件）

① markdown转PDF
Ⅰ.使用Typora 打开 markdown文件
Ⅱ.点击 文件-导出-HTML
Ⅲ.浏览器打开文件-打印-另存为PDF


② markdown转PDF文件分页
<div STYLE="page-break-after: always;"></div>

```

---

##### 4、github、gitee 仓库小问题

```
4、github、gitee 仓库小问题

Ⅰ.在gitee仓库于2021-01-12 、2021-01-26删除了一些大文件，但是这些文件还在提交记录中，仍然占空间，所以仓库显得比较大（290M）。


Ⅱ.GitHub 图片加载不出来怎么办
打开路径 C:\Windows\System32\drivers\etc 下的 hosts 文件,添加下面的语句

# GitHub Start 
192.30.253.112 Build software better, together 
192.30.253.119 gist.github.com
151.101.184.133 assets-cdn.github.com
151.101.184.133 raw.githubusercontent.com
151.101.184.133 gist.githubusercontent.com
151.101.184.133 cloud.githubusercontent.com
151.101.184.133 camo.githubusercontent.com
151.101.184.133 avatars0.githubusercontent.com
151.101.184.133 avatars1.githubusercontent.com
151.101.184.133 avatars2.githubusercontent.com
151.101.184.133 avatars3.githubusercontent.com
151.101.184.133 Build software better, together
151.101.184.133 avatars5.githubusercontent.com
151.101.184.133 avatars6.githubusercontent.comⅢ
151.101.184.133 avatars7.githubusercontent.com
151.101.184.133 Build software better, together


Ⅲ.github.io（github pages）访问不了
参考 https://blog.csdn.net/nima1994/article/details/107985198

1、修改系统的hosts （C:\Windows\System32\drivers\etc），添加

# GitHub Start 
185.199.110.153 catalyst-team.github.io
185.199.111.153 raw.githubusercontent.com

2、修改本地连接 IPv4属性
控制面板--网络和 Internet--网络和共享中心--查看活动网络--本地连接--属性--Internet 协议版本4（TCP/IPv4）--属性--使用下面的DNS服务器地址
首选DNS服务器设置为 223.5.5.5
备用DNS服务器设置为 223.6.6.6

```

---

##### 5、github、gitee、gitlab静态网页发布 

```
5、github、gitee、gitlab静态网页发布 

github pages 
位置：Setting - GitHub Pages - Save
优点：有十种主题可以选择；提交代码自动发布最新文件。
不足：主题偏少。外网，选择主题时速度慢，但是发布后的文件访问较快。
备注：删除目录下_config.yml文件即选择默认的markdown主题。


gitee pages   
位置：服务 - Gitee Pages - 启动
优点：速度快。
不足：无主题可选；markdown会还原成纯文本，需要转成html文件；提交新代码后每次需手动更新才能发布新文件。
备注：有时服务改造，无法启动。


------ 


gitlab pages   
gitlab布署网页与gitee、github不一样，无需启动。只要添加文件即可。
1、添加  .gitlab-ci.yml 文件 。
2、创建public文件夹，并将html文件上传到此处。
3、推送文件。在gitlab项目 CI/CD --> Jobs 查看是否布署成功。
4、访问页面。如 ：  https://xuyq123.gitlab.io/plain/index.html

参考项目： https://gitlab.com/xuyq123/plain

--- 

.gitlab-ci.yml 文件

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
   
   
```

---

##### 6、gitlab、github、gitee布署mkdocs主题仓库


```
mkdocs作用
在git平台创建mkdocs主题仓库，自动将markdown文件生成静态网页。 

官网  https://www.mkdocs.org/
简介&主题   https://www.jianshu.com/p/c005c45abe85
gitlab-mkdocs  https://gitlab.com/pages/mkdocs
github-mkdocs-theme  https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes

```

```
python安装及下载  https://gitlab.com/xuyq123/mynotes/-/blob/master/Python.md
mkdocs安装     pip install mkdocs

```

```
gitlab mkdocs主题仓库 （推送文件自动布署）
创建方法：
   创建 mkdocs.yml、.gitlab-ci.yml文件。创建docs目录，markdown文件放在此处。推送到仓库。
   
   可fork此仓库 https://gitlab.com/xuyq123/plain-mkdocs    https://xuyq123.gitlab.io/plain-mkdocs
   注意：若部署失败，可在 CI/CD --> Jobs查看原因，修改 .gitlab-ci.yml 文件。
   
本地调试：
	安装 python 及 mkdocs 
	相关命令 
		 pip install mkdocs      安装mkdocs
		 python -m pip install --upgrade pip     更新Python
		 
		 mkdocs new my-project
		 mkdocs serve
		 mkdocs build

参考：
	https://cloud.tencent.com/developer/article/1662592
	https://blog.csdn.net/qq_32332433/article/details/106148965
	
	
------	
	 
		 
主题安装
	pip install -i https://pypi.tuna.tsinghua.edu.cn/simple mkdocs-Bootswatch      安装多个主题
	pip install --trusted-host pypi.douban.com -i http://pypi.douban.com/simple/ mkdocs mkdocs-material   安装material主题
	pip install --trusted-host pypi.douban.com -i http://pypi.douban.com/simple/ mkdocs mkdocs-gitbook    安装gitbook主题
	
	pip install mkdocs-cluster   
	pip install mkdocs-cinder      
	pip install mkdocs-bootstrap4
		
		
主题种类					
The available installed themes are: readthedocs, cluster, lumen, flatly, yeti, material, cosmo, litera, sandstone, darkly, lux, minty, superhero, 
gitbook, spacelab, cinder, pulse, mkdocs, cerulean, bootstrap, solar, journal, simplex, slate, materia, bootstrap4, ivory, cyborg, united



主题总结
	mkdocs      默认主题，速度极快。菜单横向。蓝边白底。不够美观。     评级四星。
	material    速度一般。菜单竖向，左侧整体文件结构，右侧文件目录。紫边白底。   评级四星。
	readthedocs 速度一般。菜单竖向。展示markdown文件目录结构。黑边白底。  评级四星。
	sandstone   加载挺快。菜单横向。黑边白底。    评级四星。
	litera      加载挺快。菜单横向。天蓝边白底。  评级四星。
	lumen       加载挺快。菜单横向。海蓝边白底。  评级四星。
	spacelab    加载挺快。菜单横向。深蓝边白底。  评级四星。
	cluster	    加载挺快。菜单横向。灰边白底。    评级四星。
	slate	    加载挺快。菜单横向。黑边黑底。    评级四星。
	simplex	    加载挺快。菜单横向。深红边白底。  评级四星。
	united	    加载挺快。菜单横向。橙红边白底。  评级四星。
	cinder	    速度一般，不显示文件夹中的下拉文档。展示markdown文件目录结构。菜单横向。黑边白底。适合单级目录文件。   评级三星。
	bootstrap4  速度相当慢。菜单横向。黑边白底。  评级二星。
	gitbook     速度一般。菜单竖向，灰边白底。有版权广告去不掉。   评级二星。
	ivory       速度相当慢。菜单竖向，黑边白底。  评级二星。

	
```

```
github mkdocs主题仓库 （mkdocs gh-deploy 一键布署）
创建方法：
   1、创建 mkdocs.yml、README.md、.gitignore文件。创建docs目录，markdown文件放在此处。
   2、本地调试 mkdocs serve   访问：http://127.0.0.1:8000/  。 推送上述三个文件。
   3、一键布署 mkdocs gh-deploy  （自动生成 gh-pages分支，发布GithubPages ）
   可fork此仓库 https://github.com/scott180/plain-mkdocs 	https://scott180.github.io/plain-mkdocs/
   
本地调试：
	安装 python 及 mkdocs 
	相关命令 
		 mkdocs serve
		 mkdocs gh-deploy 
	
参考：
	https://www.cnblogs.com/paulwhw/p/12725523.html
	
```

```
gitee mkdocs主题仓库 （GiteePages服务布署目录）
创建方法：
   1、创建 mkdocs.yml。创建docs目录，markdown文件放在此处。
   2、本地调试 mkdocs serve   访问：http://127.0.0.1:8000/  。
   3、mkdocs build  生成静态文件，于 site 文件夹中。
   4、推送 mkdocs.yml 、docs、site到仓库。
   5、开启GiteePages服务，选择对应分支。
   可参考此仓库 https://gitee.com/xy180/plain-mkdocs 	http://xy180.gitee.io/plain-mkdocs/1/site
   
本地调试：
	安装 python 及 mkdocs 
	相关命令 
		 mkdocs serve
		 mkdocs build
		 
参考：
	https://www.cnblogs.com/yywBlogW/p/11362889.html

```

---

---

##### 7、名帖    

```
7、名帖    
文徵明-小楷赤壁赋、颜真卿-多宝塔碑、王羲之-兰亭集序   
```

> 文徵明-小楷赤壁赋 <br/>
![文徵明-小楷赤壁赋]( https://xyqin.coding.net/p/my/d/imgs/git/raw/master/%E4%B9%A6%E6%B3%95%E5%AD%97%E5%B8%96/文徵明-小楷赤壁赋.jpg)

> 颜真卿-多宝塔碑 <br/>
![颜真卿-多宝塔碑]( https://xyqin.coding.net/p/my/d/imgs/git/raw/master/%E4%B9%A6%E6%B3%95%E5%AD%97%E5%B8%96/颜真卿-多宝塔碑.jpg)

> 王羲之-兰亭集序 <br/>
![王羲之-兰亭集序]( https://xyqin.coding.net/p/my/d/imgs/git/raw/master/%E4%B9%A6%E6%B3%95%E5%AD%97%E5%B8%96/王羲之-兰亭集序.jpg)

---

#### 四、个人账号

> 若有志同道合的小伙伴想联系本人，可通过以下方式发邮件或私信。路漫漫其修远兮，吾将上下而求索。共勉。

- **个人邮箱** &ensp; 1021151991@qq.com 
- **资讯账号** &ensp; [头条]( https://www.toutiao.com/c/user/token/MS4wLjABAAAA2_bWhiknCbcKNu4c6VTM2B7m2vr7zBrh0x6fSyOrtGU ) &ensp;  [豆瓣]( https://www.douban.com/people/80730595/photos ) &ensp;  [知乎]( https://www.zhihu.com/people/xu-xian-sheng-72-29/posts ) &ensp; 
- **项目仓库** &ensp; [gitlab]( https://gitlab.com/xuyq123/calligraphy ) &ensp; [csdn_code]( https://codechina.csdn.net/xu180/calligraphy ) &ensp; [github]( https://github.com/scott180/calligraphy ) &ensp; [coding]( https://xyqin.coding.net/public/my/calligraphy/git ) &ensp; [gitee]( https://gitee.com/xy180/calligraphy ) &ensp; [bitbucket]( https://bitbucket.org/xu12345/calligraphy ) &ensp; [gitlab_mkdocs]( https://xuyq123.gitlab.io/plain-mkdocs/ )


***


